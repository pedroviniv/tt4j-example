/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.nlp.tt4j.example;

import org.annolab.tt4j.TokenHandler;

/**
 *
 * @author PedroArthur
 */
public class TreeTaggerHandler implements TokenHandler<String> {
    
    public StringBuilder phraseBuilder;
    
    public TreeTaggerHandler() {
        phraseBuilder = new StringBuilder();
    }

    @Override
    public void token(String token, String pos, String lemma) {
        if(pos.equals("NOM") || pos.equals("ADJ")) {
            this.phraseBuilder.append(lemma).append(" ");
        }
    }
    
    public String getLastPhrase() {
        String phrase = this.phraseBuilder.toString();
        this.phraseBuilder = new StringBuilder();
        return phrase;
    }
    
}
