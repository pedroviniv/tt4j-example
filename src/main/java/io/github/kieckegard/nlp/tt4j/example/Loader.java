/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.nlp.tt4j.example;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

/**
 *
 * @author PedroArthur
 */
public class Loader {

    private static final Logger LOG = Logger.getLogger(Loader.class.getName());
    
    public static List<String> extractTokens(String line) {
        return Arrays.asList(line.split(" "));
    }
    
    //.getArguments(), retrieve treetagger cli arguments in which the program was executed.
    //.getAdapter(), returns the token adapter, 
        //token adapter has the function to adapt each token to be processed
    //.setHandler(TokenHandler<String>), sets a handler to process each token result
        //A TokenHandler is just a consumer function which receives as parameter
        //the token, the token's tag and the token's lemma.
    //.setModel(path : String), sets a language model that will be used by treeTagger
        //to proccess the tokens. In this example we're using portuguese model.
    //.process(ArrayList<String> tokens), process a word list, each token's result 
        //will be passed by handler's callback function parameters.
    public static void main(String[] args) {
        
        File file = new File("src/main/resources/tree-tagger");
        String ttPath = file.getAbsolutePath();
        String modelPath = ttPath + "/lib/pt.par";
        
        Scanner scanner = new Scanner(System.in);
        
        System.setProperty("treetagger.home", ttPath);
        
        TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<>();
        
        try {
            
            TreeTaggerHandler handler = new TreeTaggerHandler();
          
            tt.setModel(modelPath);
            tt.setHandler(handler);
            
            
            while(true) {
                System.out.println("======================");
                System.out.println("Say something: ");
                String newLine = scanner.nextLine();
                List<String> tokens = extractTokens(newLine);
                try {
                    System.out.println("----------------------");
                    tt.process(tokens);
                    System.out.println(handler.getLastPhrase());
                } catch (TreeTaggerException ex) {
                    LOG.log(Level.SEVERE, "Coudln't process tokens " + tokens, ex);
                }
            }
            
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Coudln't find the model " + modelPath, ex);
        }
    }
}
